from argparse import ArgumentParser
from os import path

from music21 import converter

from src import paint_notes


def execute_cli_script() -> None:
    def config_cmd() -> ArgumentParser:
        config = ArgumentParser(
            description='Utility for coloring notes')
        config.add_argument('file', help='Provide a path to file with notes (e.g. .musicxml, .mxl)')
        return config

    file = config_cmd().parse_args().file
    if path.exists(file) and path.isfile(file):
        directories, filename_with_ext = path.split(file)
        filename = path.splitext(filename_with_ext)[0].replace('.', '')
        output_path = path.join(directories, filename + '_colored')
        stream = converter.parse(file)
        paint_notes(stream)
        stream.write('musicxml', fp=output_path)
    else:
        print(f'No such file: {file}')


if __name__ == '__main__':
    execute_cli_script()
